package com.example.tfc02.data

import com.example.tfc02.R
import com.example.tfc02.model.ChatChannel


object DAOChatChannelSingleton {
    private var serial: Long = 1
    private val chats = ArrayList<ChatChannel>()


    fun addChatChannel(cc: ChatChannel){
        cc.photoURI = R.drawable.avatar_img
        cc.chatName = "Chat Channel " + "${serial}"
        cc.lastMessage = "Empty..."
        this.chats.add(cc)
        cc.id = serial++
    }


    fun getChatChannels(): ArrayList<ChatChannel>{
        return this.chats
    }

    fun getChatChannelById(id: Long): ChatChannel?{
        for (cc in this.chats){
            if(cc.id == id)
                return cc
        }
        return null
    }

    fun getChatChannelPositionById(id: Long): Int{
        return this.chats.indexOf(ChatChannel(id))
    }

}