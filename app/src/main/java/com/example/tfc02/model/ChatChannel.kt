package com.example.tfc02.model

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import kotlin.random.Random

class ChatChannel (

    var photoURI: Int,
    var chatName: String,
    var lastMessage: String,
    var id: Long = 0
        ){

    @RequiresApi(Build.VERSION_CODES.O)
    var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

    constructor(id: Long): this(0, "" ,""){
        this.id = id
    }

    private val chatMessages = ArrayList<ChatMessage>()


    @RequiresApi(Build.VERSION_CODES.O)
    fun addChatMessage(text: String){
        if(text.trim() != ""){
            var timeMessage = LocalDateTime.now()
            var names = arrayOf("Marco","Carlos")
            Log.i("teste1", timeMessage.toString())
            var dateTime = timeMessage.format(this.formatter).toString()
            var personName = ""
            var sent = Random.nextBoolean()

            if(sent){
                personName = names[0]
            }else{
                personName = names[1]
            }

            var chatMessage = ChatMessage(sent,personName,dateTime, text)
            this.chatMessages.add(chatMessage)
        }
    }

    fun getChatMessages():ArrayList<ChatMessage>{
        return this.chatMessages
    }

    override fun equals(other: Any?): Boolean {
        return this.id == (other as? ChatChannel)?.id
    }

}