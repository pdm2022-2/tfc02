package com.example.tfc02.model

class ChatMessage(
    var sent: Boolean,
    var personName: String,
    val dateTimeMensagem: String = "00/00/00 00-00-00",
    var text: String,
    var id: Long = 0

) {

}