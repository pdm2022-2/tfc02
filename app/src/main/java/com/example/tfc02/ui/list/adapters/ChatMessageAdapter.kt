package com.example.tfc02.ui.list.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.tfc02.R
import com.example.tfc02.model.ChatMessage
import com.example.tfc02.ui.list.viewholders.ChatMessageViewHolder

class ChatMessageAdapter(
    private var listMessage: ArrayList<ChatMessage>
): Adapter<ChatMessageViewHolder>() {
    companion object{
        private val TYPE_SENT = 0
        private val TYPE_RECEIVED = 1
    }

    override fun getItemViewType(position: Int): Int {
        if(listMessage[position].sent){
            return TYPE_SENT
        }else{
            return TYPE_RECEIVED
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatMessageViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(
            if(viewType == TYPE_SENT)
                R.layout.itemview_sent_message
            else
                R.layout.itemview_received_message
        , parent, false)

        val viewHolder = ChatMessageViewHolder(itemView)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ChatMessageViewHolder, position: Int) {
        var chatMessage = this.listMessage[position]
        holder.bind(chatMessage)
    }

    override fun getItemCount(): Int {
        return this.listMessage.size
    }
}