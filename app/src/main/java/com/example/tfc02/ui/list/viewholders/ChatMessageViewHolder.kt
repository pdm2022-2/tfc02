package com.example.tfc02.ui.list.viewholders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.tfc02.R
import com.example.tfc02.model.ChatMessage

class ChatMessageViewHolder(
    itemView: View

):ViewHolder(itemView) {

    var txtPersonName = itemView.findViewById<TextView>(R.id.txtPersonName)

    var txtMessageText = itemView.findViewById<TextView>(R.id.txtMessageText)

    var txtMessageTime = itemView.findViewById<TextView>(R.id.txtMessageTime)

    private lateinit var currentMessage: ChatMessage

    fun bind(m: ChatMessage){
        this.currentMessage = m
        this.txtPersonName.text = m.personName
        this.txtMessageText.text = m.text
        this.txtMessageTime.text = m.dateTimeMensagem
    }
}