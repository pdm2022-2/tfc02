package com.example.tfc02.ui.list.viewholders

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.tfc02.R
import com.example.tfc02.model.ChatChannel
import com.example.tfc02.ui.list.adapters.ChatChannelAdatper

class ChatChannelViewHolder(
    itemView: View,
    protected val adapter: ChatChannelAdatper
):ViewHolder(itemView) {
    var ivChatImage = itemView.findViewById<ImageView>(R.id.ivChatImage)

    var txtChatName = itemView.findViewById<TextView>(R.id.txtChatName)

    var txtLastMessage = itemView.findViewById<TextView>(R.id.txtLastMessage)

    var llTexts = itemView.findViewById<LinearLayout>(R.id.llTexts)
    private lateinit var currentChatChannel: ChatChannel

    init {
        this.llTexts.setOnClickListener{
            this.adapter.getOnClickChatChannelListener()?.onClick(this.currentChatChannel)
        }
    }

    fun bind(cc: ChatChannel){
        this.currentChatChannel = cc
        this.ivChatImage.setImageResource(cc.photoURI)
        this.txtChatName.text = cc.chatName
        this.txtLastMessage.text = cc.lastMessage
    }
}