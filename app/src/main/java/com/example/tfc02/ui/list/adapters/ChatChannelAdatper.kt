package com.example.tfc02.ui.list.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.tfc02.R
import com.example.tfc02.model.ChatChannel
import com.example.tfc02.ui.list.viewholders.ChatChannelViewHolder

class ChatChannelAdatper (
        var listChanel: ArrayList<ChatChannel>

        ):Adapter<ChatChannelViewHolder>(){

    fun interface OnClickChatChannelListener{
        fun onClick(chatChannel: ChatChannel)
    }

    private var listener: OnClickChatChannelListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatChannelViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(R.layout.itemview_chat_channel, parent, false)

        val viewHolder = ChatChannelViewHolder(itemView, this)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ChatChannelViewHolder, position: Int) {
        var chatChannel = this.listChanel[position]
        holder.bind(chatChannel)
    }

    override fun getItemCount(): Int {
        return this.listChanel.size
    }

    fun setOnClickChatChannelListener(listener: OnClickChatChannelListener?){
        this.listener = listener
    }

    fun getOnClickChatChannelListener(): OnClickChatChannelListener?{
        return this.listener
    }
}