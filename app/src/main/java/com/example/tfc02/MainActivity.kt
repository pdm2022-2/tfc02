package com.example.tfc02

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tfc02.data.DAOChatChannelSingleton
import com.example.tfc02.model.ChatChannel
import com.example.tfc02.ui.list.adapters.ChatChannelAdatper
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {

    private lateinit var rvChatChannel: RecyclerView
    private lateinit var btnAddChatChannel: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.rvChatChannel = findViewById(R.id.rvChatChannel)
        this.btnAddChatChannel = findViewById(R.id.btnSendMessage)

        this.rvChatChannel.layoutManager = LinearLayoutManager(this)
        val adapter = ChatChannelAdatper(DAOChatChannelSingleton.getChatChannels())
        this.rvChatChannel.adapter = adapter

        fun onClickCreateChatChannel(){
            var cc = ChatChannel(0,"", "")
            DAOChatChannelSingleton.addChatChannel(cc)
            this.rvChatChannel.adapter?.notifyItemInserted(DAOChatChannelSingleton.getChatChannels().size - 1)
        }

        var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){result ->
            if(result.resultCode == RESULT_OK && result.data != null){
                val lastMessage = result.data!!.getStringExtra("lastMessage")
                if(lastMessage?.trim() != null) {
                    val chatChannelId = result.data!!.getLongExtra("ccId", -1)
                    val chatChannel = DAOChatChannelSingleton.getChatChannelById(chatChannelId)
                    chatChannel!!.lastMessage = lastMessage!!
                    val chatChannelPosition =
                        DAOChatChannelSingleton.getChatChannelPositionById(chatChannelId)
                   adapter.notifyItemChanged(chatChannelPosition.toString().toInt())
                }
            }
        }

        adapter.setOnClickChatChannelListener{ cc ->
            val openCCIntent = Intent(this, MessageActivity::class.java)
            openCCIntent.putExtra("ccId", cc.id)
            resultLauncher.launch(openCCIntent)
        }

        this.btnAddChatChannel.setOnClickListener {
            onClickCreateChatChannel()
            Log.i("teste", DAOChatChannelSingleton.getChatChannels().size.toString())
        }




    }
}