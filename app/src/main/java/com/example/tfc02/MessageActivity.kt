package com.example.tfc02

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tfc02.data.DAOChatChannelSingleton
import com.example.tfc02.model.ChatMessage
import com.example.tfc02.ui.list.adapters.ChatMessageAdapter

class MessageActivity : AppCompatActivity() {
    private lateinit var rvChatMessage: RecyclerView
    private lateinit var etxtMessageText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)

        this.rvChatMessage = findViewById(R.id.rvChatMessage)
        this.etxtMessageText = findViewById(R.id.etxtMessageText)

        var chatChannel = DAOChatChannelSingleton.getChatChannelById(intent.getLongExtra("ccId", -1))
        val chatMessages = chatChannel!!.getChatMessages()

        this.rvChatMessage.layoutManager = LinearLayoutManager(this)
        this.rvChatMessage.adapter = ChatMessageAdapter(chatMessages)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun onClickSendMessage(v: View){
        var text = this.etxtMessageText.text.toString()
        if(text.trim() != "") {
            var chatChannel =
                DAOChatChannelSingleton.getChatChannelById(intent.getLongExtra("ccId", -1))
            chatChannel!!.addChatMessage(text)
            this.rvChatMessage.adapter?.notifyItemInserted(chatChannel!!.getChatMessages().size - 1)
            this.etxtMessageText.text.clear()
        }else{
            Toast.makeText(this, "deu meme", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onBackPressed() {
        val output = Intent()
        var chatChannel = DAOChatChannelSingleton.getChatChannelById(intent.getLongExtra("ccId", -1))
        val chatMessages = chatChannel!!.getChatMessages()


        output.putExtra("ccId", intent.getLongExtra("ccId", -1))
        var chatMessage:ChatMessage = chatMessages[chatMessages.size - 1]
        var lastMessage = "${chatMessage.personName}: ${chatMessage.text}"
        output.putExtra("lastMessage", lastMessage)
        setResult(RESULT_OK, output)

        super.onBackPressed()
    }
}